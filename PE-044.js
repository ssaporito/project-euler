function minimal_pentagon_difference(n)
{		
	n=parseInt(n);
	var D=Number.POSITIVE_INFINITY;
	var mult=(n>2000)?(2000):(n-1);
	
	for(var m=1;mult*m<n;m++)
	{
		for(var k=1;k<n;k++)
		{
			var p_k=pentagonal_number(k);						
			for(var j=k+mult*(m-1);j<k+mult*m;j++)
			{				
				p_j=pentagonal_number(j);
				if(p_j-p_k>D)
				{
					console.log("Finished.");
					return D;
				}
				var sum=p_k+p_j;
				var diff=p_j-p_k;
				if((is_pentagonal(sum))&&(is_pentagonal(diff)))
				{
					console.log("k:"+(k+1)+",j:"+(j+1));
					D=(diff<D)?(diff):(D);
				}
				p_j=next_pentagonal(p_j,j);
			}
			p_k=next_pentagonal(p_k,k);			
		}
		console.log("m:"+m+",D:"+D);		
	}
	console.log("No value found.");
	return D;
}

function pentagonal_number(k)
{
	return k*(3*k-1)/2;
}
function next_pentagonal(p,k)
{
	return p+3*k+1;
}
function is_pentagonal(num)
{
	var m=(1+Math.sqrt(1+24*num))/6;
	return Number.isInteger(m);
}