function placeholder(problem)
{
	var placeholder="";
	switch(problem)
	{
		case "001":
		{
			placeholder="n factor1 ... factorK";
			break;
		}
		case "008":
		{
			placeholder="n\ne1...eM\ne(M+1)...e(2M)\n. . .\ne(N-M+1)...eN";
			break;
		}
		case "011":
		{
			placeholder="n\nE11...E1m\n. . .\nEn1...Emn";
			break;
		}
		case "013":
		{
			placeholder="E11...E1m\n. . .\nEn1...Emn";
			break;
		}
		case "015":
		{
			placeholder="m n k";
			break;
		}
		case "018":
		case "067":
		{
			placeholder="e.g. \n75\n95 64\n17 47 82"
			break;
		}
		case "023":
		case "034":
		case "035":
		case "037":
		{
			placeholder="upper-bound";
			break;
		}
		case "024":
		{
			placeholder="n k";
			break;
		}
		case "027":
		{
			placeholder="max_a max_b";
			break;
		}
		case "030":
		{
			placeholder="power";
			break;
		}
		case "031":
		{
			placeholder="value";
			break;
		}
		case "041":
		{
			placeholder="number_of_digits";
			break;
		}	
		case "044":		
		{
			placeholder="max_n";
			break;
		}	
		case "045":
		{
			placeholder="nth_next";
			break;
		}	
		case "061":
		{
			placeholder="set_size";
			break;
		}	
		case "064":
		{
			placeholder="max_N";
			break;
		}
		case "Noisy":
		{
			placeholder="R C N";
			break;
		}
		default:
		{
			placeholder="n";
			break;
		}
	}
	return placeholder;	
}