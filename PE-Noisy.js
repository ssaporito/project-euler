function minimum_unhappiness(R,C,N)
{
	return minimum_unhappiness_v1(R,C,N);
}
function minimum_unhappiness_v1(R,C,N)
{
	function removed_unhappiness(R,C,K,n_inside,n_corners) {
	  if (K <= n_inside) return 4*K;
	  var n_sides = Math.ceil((R*C)/2) - n_inside - n_corners;	  
	  if (K <= n_inside + n_sides) return 4*n_inside + 3*(K-n_inside);
	  return 4*n_inside + 3*n_sides + 2*(K-n_inside-n_sides);
	}
	if (R > C) 
	{
		var aux=C;
		C=R;
		R=aux;
	}
    if (N <= R*C/2) {
      return 0;
    }
    var starting_unhappiness = (R-1)*C + (C-1)*R;
    var K = R*C-N;
    if (R == 1) {
      return starting_unhappiness-2*K;      
    }
    var n_inside = Math.ceil((R-2)*(C-2)/2);
    var n_corners = ((C%2==1)&&(R%2==1))?(4):(2);
    var ret = starting_unhappiness - Math.max(removed_unhappiness(R,C,K,n_inside, n_corners),removed_unhappiness(R,C,K,(R-2)*(C-2)-n_inside, 4-n_corners));
    return ret;
}

function minimum_unhappiness_line(line)
{
	var params=line.split(" ");
	var R=parseInt(params[0]);
	var C=parseInt(params[1]);
	var N=parseInt(params[2]);
	return minimum_unhappiness(R,C,N);
}
