function sum_convergent_digits(n)
{
	var m=n-1;
	var digits_array=(N(m).c);
	var sum=0;
	for(var k in digits_array)
	{
		sum+=sum_digits(digits_array[k]);
	}
	return sum;
}
var memo_N={};
var N=function(k)
{
	if(memo_N[k])return memo_N[k];
	if(k==0)return memo_N[k]=new BigNumber(e_a(0));
	if(k==1)return memo_N[k]=new BigNumber(e_a(0)*e_a(1)+1);
	return memo_N[k]=N(k-1).times(e_a(k)).plus(N(k-2));
}
var memo_e_a={};
var e_a=function(k)
{
	if(memo_e_a[k])return memo_e_a[k];
	if(k==0)return memo_e_a[k]=2;
	if(k%3==2)
	{
		var m=(k-2)/3;
		return memo_e_a[k]=2*(m+1);
	}
	else return memo_e_a[k]=1;
}
