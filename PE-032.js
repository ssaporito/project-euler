function is_pandigital(number,n)
{
	var factors=divisors(number);
	var factors_length=factors.length;
	for(var i=0;i<factors_length;i++)
	{
		var complement=number/factors[i];
		var is_pandigital=true;
		var iden_string=factors[i].toString()+complement.toString()+number.toString();
		if(iden_string.length==n)
		{
			for(var k=1;k<=n;k++)
			{					
				if(count_occurrences(iden_string,k.toString(),true)!==1)
				{
					is_pandigital=false;
					break;
				}
			}
			if(is_pandigital)
			{
				console.log("i:"+factors[i].toString()+",j:"+complement.toString()+",number:"+number.toString());						
				return true;
			}
		}
	}
	return false;
}
function sum_pandigital_products(n)
{
	var sum=0;
	var limit=Math.pow(10,Math.ceil(n/2)-1);	
	for(var k=1;k<limit;k++)
	{
		if(is_pandigital(k,n))
		{
			sum+=k;
		}
	}
	return sum;
}
