function create_results_table(results)
{
	$("#results").html("");
	$("#results").append("<tr><th>Input</th><th>Result</th><th>Time</th></tr>");
	for(var input in results)
	{		
		var row="<tr>";
		row+="<td><input type='hidden' value='"+input+"'>"+input.substr(0,10)+((input.length>10)?("..."):(""))+"</input></td>";
		row+="<td>"+results[input].result+"</td>";
		row+="<td>"+results[input].time+"</td>";
		row+="</tr>";
		$("#results").append(row);
	}		
	$("#results").css("table-layout","fixed");
}	