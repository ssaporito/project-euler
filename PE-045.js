function next_multipoly_trio(n)
{
	n=parseInt(n);
	var t=285;
	//var p=165;
	//var h=143;
	var count=0;
	while(true)
	{		
		var number=polygonal_number(t,3);
		if(is_polygonal(number,5))
		{
			if(is_polygonal(number,6))
			{				
				if(count==n)
				{
					return number;
				}
				count++;
			}
		}	
		t++;
	}
}