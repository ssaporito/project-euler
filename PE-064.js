function count_odd_period_fractions(N)
{	
	N=parseInt(N);
	var count=0;
	for(var i=2;i<=N;i++)
	{		
		var period=find_period(i);
		if(period%2==1)
		{
			count++;
		}
	}
	return count;
}
function find_period(N)
{
	var memo_a={};
	var a=function(k)
	{			
		return memo_a[k]||(memo_a[k]=Math.floor(r(k)));
	}
	var memo_b={};
	var b=function(k)
	{
		if(k==0)return memo_b[k]=0;
		return memo_b[k]||(memo_b[k]=d(k-1)*a(k-1)-b(k-1));
	}
	var memo_d={};
	var d=function(k)
	{	
		if(k==0)return memo_d[k]=1;
		return memo_d[k]||(memo_d[k]=(N-Math.pow(b(k),2))/d(k-1));
	}
	var memo_r={};
	var N_sqrt=Math.sqrt(N);
	var r=function(k)
	{	
		return memo_r[k]||(memo_r[k]=(N_sqrt+b(k))/d(k));
	}
	var k=0;
	//var period=[];
	
	if(Number.isInteger(N_sqrt))return 0;
	while(true)
	{
		var coeff_b=b(k);
		for(var i=0;i<k;i++)
		{
				if(coeff_b==b(i))
				{
					var coeff_d=d(k);
					if(coeff_d==d(i))
					{		
						/*for(var m=i;m<k;m++)
						{
							period.push(a(m));
						}*/
						return k-i;
					}
				}
		}
		k++;
	}
	
}