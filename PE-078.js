function least_divisible_partition(n)
{
	var i=0;
	var p=[1];
	while(p[i]!=0)
	{
		i++;
		var sum=0;
		var pentagonal=0;		
		var k=1;
		while (pentagonal<=i)
		{
			pentagonal=polygonal_number(k,5);					
			var sign=(((k-1)%2==0)?(1):(-1));
			if (pentagonal<=i)
			{				
				sum+=(sign*(p[i-pentagonal]));
			}
			if(pentagonal+k<=i)
			{				
				sum+=sign*p[i-(pentagonal+k)]; 
			}										
			k++;
		}
		
		p.push(sum%n);		
	}	
	return i;
}