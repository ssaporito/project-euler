function consecutive_primes(rad_a,rad_b)
{	
	var b_list=primes_list(rad_b);	
	var max_n=0;
	var max_a=0;
	var max_b=0;
	for(var a=-rad_a;a<=rad_a;a++)
	{
		for(var key in b_list)
		{			
			var b=b_list[key];
			var n=0;	
			var sum=b;		
			while (is_prime(sum))
			{								
				n++;		
				if(n>max_n)
				{
					max_n=n;
					max_a=a;
					max_b=b;
				}		
				sum=n*n+n*a+b;
			}			
		}
	}
	return max_a*max_b;
}

function consecutive_primes_line(line)
{
	var params=line.split(" ");
	var rad_a=parseInt(params[0]);
	var rad_b=parseInt(params[1]);
	return consecutive_primes(rad_a,rad_b);
}