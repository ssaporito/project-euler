function digit_cancelling_product(n)
{	
	n=parseInt(n);
	var upper_bound=Math.pow(10,n);
	var found_fractions=[];
	for(var k=Math.pow(10,n-1);k<upper_bound-1;k++)
	{
		for(var l=k+1;l<upper_bound;l++)
		{
			var fraction=digit_cancelling_fraction(k,l);
			if(fraction)
			{
				found_fractions.push(fraction);
			}
		}
	}
	//console.log(found_fractions);
	if(found_fractions.length>0)
	{
		var reverse_product=1;	
		for(var k=0;k<found_fractions.length;k++)
		{
			reverse_product*=found_fractions[k][1]/found_fractions[k][0];
		}
		return reverse_product;
	}
	return 0;
}

function digit_cancelling_fraction(k,l)
{
	var fraction=k/l;
	var k_str=k.toString();
	var l_str=l.toString();	
	var k_str_length=k_str.length;
	var l_str_length=l_str.length;
	var k_zero_index=k_str.lastIndexOf("0");
	var l_zero_index=l_str.lastIndexOf("0");
	if((k_zero_index==k_str_length-1)&&(l_zero_index==l_str_length-1))
	{
		return false;
	}
	for(var i=0;i<k_str_length;i++)
	{
		for(var j=0;j<l_str_length;j++)
		{
			if(k_str[i]==l_str[j])
			{				
			
				var cancelled_k=k_str.slice(0,i)+k_str.slice(i+1);
				var cancelled_l=l_str.slice(0,j)+l_str.slice(j+1);	
				//console.log("k:"+k_str+",k_i:"+k_str[i]+",k_can:"+cancelled_k);
				//console.log("l:"+l_str+",l_j:"+l_str[j]+",l_can:"+cancelled_l);
				if(parseInt(cancelled_k)/parseInt(cancelled_l)==fraction)
				{
					return [cancelled_k,cancelled_l];
				}
			}
		}
	}
	return false;
}