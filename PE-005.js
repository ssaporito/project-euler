function multiply_missing_factors(a_factorized,n)
{
	var n_factorized=reduced_factorize(n);
	var missing_factors={};	
	var result=a_factorized;
	for(var factor in n_factorized)
	{		
		if(factor in a_factorized)
		{
			if(n_factorized[factor]>a_factorized[factor])
			{
				result[factor]+=n_factorized[factor]-a_factorized[factor];
			}
		}
		else
		{
			missing_factors[factor]=n_factorized[factor];
			result[factor]=n_factorized[factor];
		}
	}
}
function smallest_evenly_divisible(n)
{
	return smallest_evenly_divisible_v2(n);
}
function smallest_evenly_divisible_v1(n)
{
	var a=n;	
	while(true)
	{
		var m=n;		
		while (a%m==0)
		{			
			m--;			
			if(m==1)
			{
				return a;
			}
		}
		a+=n;
	}
}
function smallest_evenly_divisible_v2(n)
{
	var a_factors={};
	a_factors[1]=1;
	for(var i=1;i<=n;i++)
	{
		multiply_missing_factors(a_factors,i);
	}
	var a=1;
	for(var factor in a_factors)
	{
		a*=Math.pow(factor,a_factors[factor]);
	}
	return a;
}