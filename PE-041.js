function largest_pandigital_prime(n)
{	
	n=parseInt(n);
	if((n>9)||(n<1))
	return "Invalid n";
	else
	{
		var max=Math.pow(10,n);
		var p_list=primes_list(max);
		var p_list_length=p_list.length;
		for(var i=p_list_length-1;i>=0;i--)
		{
			if(is_pandigital(p_list[i]))
			{
				return p_list[i];
			}
		}
		return "None.";
	}
}

function is_pandigital(number)
{	
	var num_string=number.toString();
	var num_len=num_string.length;
	for(var k=1;k<=num_len;k++)
	{			
		if(!contains(num_string,k.toString()))
		{
			return false;
		}
	}
	return true;
}

