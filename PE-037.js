function sum_truncatable_primes(n)
{
	n=parseInt(n);
	var sieve_pack=primes_sieve(n);	
	var sieve=sieve_pack["map"];
	var list=sieve_pack["list"];
	var list_length=list.length;
	var truncatable_primes=[];
	for(var i=0;i<list_length;i++)
	{
		var p=list[i];
		if(is_both_truncatable(sieve,p))
		{
			truncatable_primes.push(p);
		}
	}
	console.log(truncatable_primes);
	return truncatable_primes.reduce(function(a,b){return a+b;},0);
}

function is_both_truncatable(sieve,p)
{
	var p_str=p.toString();
	var p_str_length=p_str.length;
	for(var k=1;k<p_str_length;k++)
	{
		if(sieve[parseInt(p_str.slice(k))]==0)
		{						
			return false;
		}
	}
	for(var k=p_str_length-1;k>0;k--)
	{
		if(sieve[parseInt(p_str.slice(0,k))]==0)
		{		
			return false;
		}
	}	
	return true;
}
