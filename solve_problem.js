function solve_problem(problem,input)
	{	
		var inputs;
		
		switch(problem)
		{
			default:
			{
				inputs=input.split("\n");
				break;
			}
			case "008":
			{
				inputs=input.split("\n\n");		
				var n=0;				
				var inputs_length=inputs.length;
				for(var i=0;i<inputs_length;i++)
				{
					var curr=inputs[i];
					var curr_lines=curr.split("\n");
					var n=parseInt(curr_lines[0]);
					var number=curr_lines.slice(1).join("").replace(/(\r\n|\n|\r)/gm,"");
					inputs[i]={"n":n,"number":number};						
				}
				break;
			}
			case "011":
			{
				inputs=input.split("\n\n");		
				var n=0;				
				var inputs_length=inputs.length;
				for(var i=0;i<inputs_length;i++)
				{
					var curr=inputs[i];
					var curr_lines=curr.split("\n");
					var n=parseInt(curr_lines[0]);
					var matrix=curr_lines.slice(1);
					inputs[i]={"n":n,"matrix":matrix};						
				}
				break;
			}
			case "013":
			case "018":			
			case "067":
			{
				inputs=input.split("\n\n");				
				break;
			}
		}
		var inputs_length=inputs.length;
		var results={};
		var func;
		switch(problem)
		{		
			case "001":
			{				
				func=sum_multiples_line;
				break;
			}
			case "002":
			{
				func=sum_multiples_fibonacci_2;
				break;
			}
			case "003":
			{
				func=largest_prime_factor;
				break;
			}
			case "004":
			{
				func=largest_palindrome_product;
				break;
			}
			case "005":
			{
				func=smallest_evenly_divisible;
				break;
			}
			case "006":
			{
				func=diff_sum_power_2;
				break;
			}
			case "007":
			{
				func=find_nth_prime;
				break;
			}
			case "008":
			{
				func=greatest_adjacent_product_matrix;
				break;
			}
			case "009":
			{
				func=pythagorean_triplet;
				break;
			}
			case "010":
			{
				func=sum_primes;
				break;
			}
			case "011":
			{
				func=highest_product_grid_n;
				break;
			}
			case "012":
			{
				func=lowest_divisible_triangular;
				break;
			}
			case "013":
			{
				func=large_sum_2;
				break;
			}
			case "014":
			{
				func=longest_collatz_sequence;
				break;
			}
			case "015":
			{
				func=lattice_paths_line;
				break;
			}
			case "018":
			case "067":
			{
				func=max_path_sum;
				break;
			}
			case "023":
			{
				func=sum_nonabundant;
				break;
			}
			case "024":
			{
				func=find_nth_permutation_line;
				break;
			}
			case "025":
			{
				func=first_n_digit_fibonacci;
				break;
			}
			case "026":
			{
				func=longest_recurring_cycle;
				break;
			}
			case "027":
			{
				func=consecutive_primes_line;
				break;
			}
			case "028":
			{
				func=sum_diagonals_spiral;
				break;
			}
			case "030":
			{
				func=sum_power_digits_line;
				break;
			}
			case "031":
			{
				func=count_coin_sums;
				break;
			}
			case "032":
			{
				func=sum_pandigital_products;
				break;
			}
			case "033":
			{
				func=digit_cancelling_product;
				break;
			}
			case "034":
			{
				func=sum_digit_factorials;
				break;
			}
			case "035":
			{
				func=count_circular_primes;
				break;
			}
			case "037":
			{
				func=sum_truncatable_primes;
				break;
			}
			case "041":
			{
				func=largest_pandigital_prime;
				break;
			}
			case "044":
			{
				func=minimal_pentagon_difference;
				break;
			}
			case "045":
			{
				func=next_multipoly_trio;
				break;
			}
			case "061":
			{
				func=cyclical_figurate_sum;
				break;
			}
			case "064":
			{
				func=count_odd_period_fractions;
				break;
			}
			case "065":
			{
				func=sum_convergent_digits;
				break;
			}
			case "066":
			{
				func=max_minimal_solution;
				break;
			}
			case "078":
			{
				func=least_divisible_partition;
				break;
			}
			case "091":
			{
				func=count_right_triangles;
				break;
			}
			case "094":
			{
				func=sum_almost_equilateral;
				break;
			}
			case "Noisy":
			{
				func=minimum_unhappiness_line;
				break;
			}
		}
		for(var i in inputs)
		{
			var start = window.performance.now();
			switch(problem)
			{
				case "008":
				case "011":
				{
					results[i]={};
					results[i].result=func(inputs[i]);
					results[i].time=format_time((window.performance.now()-start));
					break;
				}
				default:
				{
					results[inputs[i]]={};
					results[inputs[i]].result=func(inputs[i]);
					results[inputs[i]].time=format_time((window.performance.now()-start));
				}
			}			
		}
		//console.log(results);
		return results;		
	}