
function find_nth_permutation(n,k)
{
	var chars=[];
	for(var i=0;i<k;i++)
	{
		chars.push(i);
	}
	var perm_memo={};	
	var lexicographic_permutation=function(chars,r)
	{
		if(r==0)
		{
			return chars.join("");
		}
		var k=chars.length;	
		if(k==0)
		{
			return "";
		}
		var m=Math.floor(r/factorial(k-1));		
		var curr=chars[m];
		chars.splice(m,1);
		console.log(chars);
		return curr+lexicographic_permutation(chars,r%factorial(k-1));
	}
	return lexicographic_permutation(chars,n);
}

function find_nth_permutation_line(line)
{
	var params=line.split(" ");
	var n=params[0];
	var k=params[1];
	return find_nth_permutation(n,k);
}