function sum_almost_equilateral(N)
{
	N=parseInt(N);
	return sum_almost_equilateral_v3(N);
}
function sum_almost_equilateral_v3(N)
{	
	var sum=0;
	for(var k=2;3*a(k)+1<=N;k++)
	{
		sum+=(k%2==0)?(3*a(k)+1):(3*a(k)-1);
		//console.log(a(k));
	}	
	return sum;
}

var memo_a={};
function a(n)
{
	if((n==0)||(n==1))return 1;
	if(n==2)return 5;
	return (memo_a[n]||(memo_a[n]=3*a(n-1)+3*a(n-2)-a(n-3)));
}





function sum_almost_equilateral_v2(N)
{	
	var N_mod3=N%3;
	var N_near3=N-N_mod3;
	var x1_max=(N_near3+1<=N)?((N_near3)/3):((N_near3-3)/3);
	//var x_k2=(N_near3+2<=N)?((N_near3+3)/3):(N_near3/3);	
	//x_k1+3*x_k1*(x_k1+1)/2;
	var sum=0;
	var odd=5;
	var square=9;
	var x1=0;
	var x2=0;
	while(x1<=x1_max)
	{		
		var delta_sqrt=Math.sqrt(4+3*square);
		x1=(2+2*delta_sqrt)/6;
		x2=x1-(2/3);
		if (Number.isInteger(delta_sqrt))
		{			
			if(Number.isInteger(x1))
			{
				sum+=3*x1+1;
				console.log("x1:"+x1+",area:"+((x1+1)/4)*Math.sqrt((3*x1+1)*(x1-1)));
			}
			else
			{					
				if(Number.isInteger(x2))
				{
					sum+=3*x2-1;
					console.log("x2:"+x2+",area:"+((x2-1)/4)*Math.sqrt((3*x2-1)*(x2+1)));
				}
			}			
		}
		odd+=2;
		square+=odd;
	}
	return sum;
}

function sum_almost_equilateral_v1(N)
{
	if(N<4)return 0;
	var N_mod3=N%3;
	var N_near3=N-N_mod3;
	var x_k1=(N_near3+1<=N)?((N_near3)/3):((N_near3-3)/3);
	var x_k2=(N_near3+2<=N)?((N_near3+3)/3):(N_near3/3);
	//x_k1+3*x_k1*(x_k1+1)/2;
	var sum=0;
	for(var k=1;k<=x_k2;k++)
	{		
		/*if((Number.isInteger(Math.sqrt((3*k+1)*(k-1)))))
		{		
			console.log("k:"+k);
			//sum+=3*k+1;
		}*/
		if((k<=x_k1)&&(Number.isInteger(Math.sqrt((3*k+1)*(k-1)))))
		{		
			console.log("k1:"+k+",area:"+((k+1)/4)*Math.sqrt((3*k+1)*(k-1)));
			sum+=3*k+1;
		}
		if((k!=1)&&(Number.isInteger(Math.sqrt((3*k-1)*(k+1)))))
		{
			console.log("k2:"+k+",area:"+((k-1)/4)*Math.sqrt((3*k-1)*(k+1)));
			sum+=3*k-1;
		}
	}
	//console.log(x_k1+","+x_k2)
	//var sum=sum1+sum2;
	return sum-4;
}

// GERAR TODOS OS QUADRADOS PERFEITOS?