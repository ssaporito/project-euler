function sum_power_digits(power)
{
	var upper_limit=(power+1)*Math.pow(9,power); // e.g. 354294=6*9^5
	var sum=0;
	for(var k=Math.pow(2,power);k<upper_limit;k++)
	{			
		var k_str=k.toString();
		var k_str_len=k_str.length;
		var sum_power_digits=0;
		for(var j=0;j<k_str_len;j++)
		{
			sum_power_digits+=Math.pow(parseInt(k_str[j]),power);
		}
		if (sum_power_digits==k)
		{	
			//console.log(k);
			sum+=k;
		}
	}
	return sum;
}

function sum_power_digits_line(line)
{
	var params=line.split(" ");
	var power=parseInt(params[0]);
	return sum_power_digits(power);
}