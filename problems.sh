rm -f problem_list.js
dirlist=(`ls | grep "PE-.*.js"`)
problems=""
for FILE in "${dirlist[@]}"
do	
	if [ ! -z "$problems" -a "$problems" != " " ]; then
        problems+=","
	fi
	i=$((${#FILE}-6))
    problems+="'${FILE:3:$i}'"
done
echo "function problem_list(){return [$problems];}">>problem_list.js