function readSingleFile(evt) 
{
	var f = evt.target.files[0]; 
	if (f) {
	  var r = new FileReader();
	  r.onload = function(e) { 
		  var contents = e.target.result; 
		  $("#input").val(contents);
		  //resize_text_area($("#input"));
	  }
	  r.readAsText(f);
	} else { 
	  console.log("Failed to load file");
	}
}  
function gcd(a,b)
{
	return greatest_common_divisor(a, b);
}
function greatest_common_divisor(a, b)
{
    while (b != 0)
	{
       var t = b;
       b = a % b;
       a = t;
	}
    return a;
}
function resize_text_area(object)
{
	if(object.val()!="")
	{
		var pre_matrix=object.val().split("\n");
		var matrix=new Array(pre_matrix.length-1);
		for(var i=0;i<pre_matrix.length-1;i++)
		{
			matrix[i]=pre_matrix[i].split(" ");
		}		
		object.attr("rows",matrix.length);
		//object.css("width",matrix[0].length*30+"px");
	}
}	
function format_time(time)
{
	var split_time=time.toString().split(".");
	if(split_time.length>1)
	{
		var significand=split_time[0].substr(0,12);
		var mantissa=split_time[1].substr(0,6);
		if(significand.length>=4)
		{
			var new_significand=significand.substr(0,significand.length-3);
			var new_mantissa=significand.substr(significand.length-3,significand.length);
			return new_significand+","+(new_mantissa+mantissa).substr(0,6)+"s";
		}
		else return significand+","+mantissa+"ms";
	}
	else return time+"ms";
}
function is_prime(n)
{
	if(n<=1)
	{
		return false;
	}
	var sqrtN=Math.sqrt(n);
	for(var m=2;m<=sqrtN;m++)
	{
		if (n%m==0)
		{
			return false;
		}
	}
	return true;
}
function primes_sieve(n)
{
	return primes_sieve_pre_filtered(n,function(){return true;});
}
function primes_sieve_pre_filtered(n,filter_func)
{
	var max_value=n;
	var sieve=[];
	var list=[];
	var sqrt_max_value=Math.sqrt(max_value);
	sieve[0]=0;
	sieve[1]=0;
	for (var i = 2; i <= max_value; i++) 
	{
		sieve[i] = 1;
	}
	for(var i=2;i<=max_value;i++)
	{	
		if(sieve[i]==1)
		{			
			if (i<sqrt_max_value)
			{
				for(var j=i*i;j<=max_value;j+=i)
				{	
					sieve[j]=0;			
				}
			}	
			if(filter_func(i))
			{
				list.push(i);						
			}
			else
			{
				sieve[i]=0;	
			}
		}
	}
	return {"map":sieve,"list":list};
}
function primes_sieve_post_filtered(n,filter_func)
{
	sieve_pack=primes_sieve(n);
	sieve=sieve_pack["map"];
	list=sieve_pack["list"];
	var list_length=list.length;
	var new_list=[];
	for(var i=0;i<list_length;i++)
	{
		if(filter_func(list[i]))
		{
			new_list.push(list[i]);
		}
		else
		{
			sieve[list[i]]=0;
		}
	}
	return {"map":sieve,"list":new_list};
}
function primes_list(n)
{	
	sieve_pack=primes_sieve(n);
	return sieve_pack["list"];
}

function dot_product(a,b)
{
   		var dot=0;		
   		for(k=0;k<a.length;k++)
   		{
   			dot+=a[k]*b[k];
   		}
   		return dot;
}
function is_right_triangle(p1,p2,p3)
{
	return is_right_triangle_v2(p1,p2,p3);
}
function is_right_triangle_v1(p1,p2,p3)
{	
	var v1=[p1[0]-p3[0],p1[1]-p3[1]];
	var v2=[p2[0]-p3[0],p2[1]-p3[1]];
	var v3=[p2[0]-p1[0],p2[1]-p1[1]];	
	if((dot_product(v1,v2)==0)||(dot_product(v1,v3)==0)||(dot_product(v2,v3)==0))
	{
		return true;
	}
	else
	{
		return false;
	}
}
function is_right_triangle_v2(p1,p2,p3)
{
	var sides=new Array(3);
	sides[0]=((p1[0]-p3[0])*(p1[0]-p3[0])+(p1[1]-p3[1])*(p1[1]-p3[1]));
	sides[1]=((p2[0]-p3[0])*(p2[0]-p3[0])+(p2[1]-p3[1])*(p2[1]-p3[1]));
	sides[2]=((p2[0]-p1[0])*(p2[0]-p1[0])+(p2[1]-p1[1])*(p2[1]-p1[1]));
	if(sides[0]>sides[1]&&sides[0]>sides[2])
	{
		return (sides[0]==sides[1]+sides[2]);
	}
	else if(sides[1]>sides[0]&&sides[1]>sides[2])
	{
		return (sides[1]==sides[0]+sides[2]);
	}
	else
	{
		return (sides[2]==sides[0]+sides[1]);
	}
}

function least_factor(n) {
 if (isNaN(n) || !isFinite(n)) return NaN;  
 if (n==0) return 0;  
 if (n%1 || n*n<2) return 1;
 if (n%2==0) return 2;  
 if (n%3==0) return 3;  
 if (n%5==0) return 5;  
 var sqrt_n = Math.sqrt(n);
 for (var i=7;i<=sqrt_n;i+=30) {
  if (n%i==0)      return i;
  if (n%(i+4)==0)  return i+4;
  if (n%(i+6)==0)  return i+6;
  if (n%(i+10)==0) return i+10;
  if (n%(i+12)==0) return i+12;
  if (n%(i+16)==0) return i+16;
  if (n%(i+22)==0) return i+22;
  if (n%(i+24)==0) return i+24;
 }
 return n;
}

function factor_into_map(a,map)
{
	map[a]=(map[a])?(map[a]+1):(1);
	return map;
}
function factorize(n) {
 var min_factor = least_factor(n);
 if (n==min_factor) return ''+n;
 return min_factor+"*"+factorize(n/min_factor);
}
function reduced_factorize(n)
{
	return reduced_factorize_v3(n);
}
function reduced_factorize_v1(n)
{	
	var f=factorize(n);
	var f_array=f.split("*");
	var result={};
	for (var i=0;i<f_array.length;i++)
	{
		result[f_array[i]]=(f_array[i] in result)?(result[f_array[i]]+1):(1);
	}
	return result;
}
function reduced_factorize_v2(n) // This is too slow
{
	var primes = primes_list(n);
	var m=n;
	var result={};
	for (var k=0;k<primes.length;k++)
	{
		var p=primes[k];
		while ((m%p) == 0)
		{
		  m /= p
		  result[p]=(p in result)?(result[p]+1):(1);
		}
		if (m == 1)
		{
		  return result;
		}
    }
}
function reduced_factorize_v3(n)
{
	var min_factor = least_factor(n);	
	if (n==min_factor) 
	{
		var obj={};
		obj[n]=1;
		return obj;
	}
	return factor_into_map(min_factor,reduced_factorize_v3(n/min_factor));
}
function count_divisors(n)
{	
	var rf=reduced_factorize(n);
	var result=1;	
	for(var divisor in rf)
	{
		result*=rf[divisor]+1;
	}
	return result;
}
function sum_proper_divisors(n) // problem 23
{
	var factors=reduced_factorize(n);
	var result=1;
	for(var prime in factors)
	{
		var current_sum=0;
		for(var k=0;k<=factors[prime];k++)
		{
			current_sum+=Math.pow(prime,k);
		}
		result*=current_sum;
	}
	return result-n;
}
function divisors(n)
{
	var prime_factors=reduced_factorize(n);		
	var factors=[];
	var multiplicities=[];	
	for(var key in prime_factors)
	{
		factors.push(key);
		multiplicities.push(prime_factors[key]);
	}
	var find_divisors=function(factors,multiplicities,current_index,current_result,current_divisors)
	{
		if (current_index == factors.length) {
			current_divisors.push(current_result);
		}
		for (var i=0; i<=multiplicities[current_index];i++) {
			find_divisors(factors, multiplicities, current_index+1,current_result,current_divisors);
			current_result *= factors[current_index];
		}
	}
	var divisors_array=[];
	find_divisors(factors,multiplicities,0,1,divisors_array);
	return divisors_array;
}

function count_occurrences(string, sub_string, allow_overlapping){

    string+=""; sub_string+="";
    if(sub_string.length<=0) return string.length+1;

    var n=0, pos=0;
    var step=(allow_overlapping)?(1):(sub_string.length);

    while(true){
        pos=string.indexOf(sub_string,pos);
        if(pos>=0){ n++; pos+=step; } else break;
    }
    return n;
}
function contains(string,sub_string)
{
	return (string.indexOf(sub_string)>=0);
}

function base_log(base, y) {
  return Math.log(y) / Math.log(base);
}
function discrete_log_of_prime(a,prime)
{
	var k=1;
	var min_k=k;
	var b=a;
	while(k<prime-1)
	{		
		k++;
		b=(b*a)%prime;		
		if(b==1)
		{
			min_k=k;
			break;
		}		
	}
	return min_k;
}
var fact_memo={};
var factorial=function(n)
{
	if((n==1)||(n==0)) return 1;
	return (fact_memo[n]||(fact_memo[n]=factorial(n-1)*n));
}

function left_rotate_string(str)
{
	return (str).slice(1)+(str).substr(0,1);
}

function polygonal_number(n,poly)
{
	switch(poly)
	{
		case 3:
		{
			return n*(n+1)/2;
		}
		case 4:
		{
			return n*n;
		}
		case 5:
		{
			return n*(3*n-1)/2;
		}
		case 6:
		{
			return n*(2*n-1);
		}
		case 7:
		{
			return n*(5*n-3)/2;
		}
		case 8:
		{
			return n*(3*n-2);
		}
		default:
		{
			return 0;
		}
	}
	
}
function inverse_polygonal_number(p,poly)
{
	switch(poly)
	{
		case 3:
		{
			return (-1+Math.sqrt(1+8*p))/2;
		}
		case 4:
		{
			return Math.sqrt(p);
		}
		case 5:
		{
			return (1+Math.sqrt(1+24*p))/6;
		}
		case 6:
		{
			return (1+Math.sqrt(1+8*p))/4;
		}
		case 7:
		{
			return (3+Math.sqrt(40*p+9))/10;
		}
		case 8:
		{
			return (1+Math.sqrt(3*p+1))/3;
		}
		default:
		{
			return 0;
		}
	}	
}

function is_polygonal(p,poly)
{
	return Number.isInteger(inverse_polygonal_number(p,poly));
}
function sum_digits(a)
{
	var sum=0;
	while(a>0)
	{
		var d=a%10;		
		a=(a-d)/10;
		sum+=d;
	}
	return sum;
}
function load_js(filename){    
        var fileref=document.createElement('script');
        fileref.setAttribute("type","text/javascript");
        fileref.setAttribute("src", filename);        
        if (typeof fileref!="undefined")
        {        	
        	document.getElementsByTagName("head")[0].appendChild(fileref);
        	console.log("File "+filename+" loaded.");
        }        
}
function $_GET(q,s) {
    s = s || window.location.search;
    var re = new RegExp('&'+q+'=([^&]*)','i');
    return (s=s.replace(/^\?/,'&').match(re)) ? s=s[1] : s='';
}