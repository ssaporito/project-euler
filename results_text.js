function create_results_text(results)
{
	$("#results").val("");	
	var i=1;
	var text="";
	for(var input in results)
	{		
		var row="Case #"+i+": "+results[input].result+"\n";
		text+=row;
		i++;
	}	
	$("#results").val(text);
	$("#results").attr("rows",i);
}	