function max_minimal_solution(n)
{
	var max=0;
	var D=null;
	for(var i=0;i<n;i++)
	{
		//console.log(i);
		var minimal=minimal_solution(i);
		if(minimal>max)
		{
			max=minimal;		
			D=i;
		}	
	}
	return D;
}
function minimal_solution(D)
{
	var D_big=new BigNumber(D);
	var D_sqrt=D_big.sqrt();
	var is_square=D_sqrt.isInteger();
	if(is_square)return "None";
	//D_sqrt=D_sqrt.floor();
	var big_1=new BigNumber(1);
	var memo_h={};	
	var h=function(k)
	{
		if(memo_h[k])return memo_h[k];
		if(k==0)return memo_h[k]=new BigNumber(a(0));
		if(k==1)return memo_h[k]=new BigNumber(a(0).times(a(1)).plus(1));
		return memo_h[k]=h(k-1).times(a(k)).plus(h(k-2));
	}
	var memo_g={};
	var g=function(k)
	{
		if(memo_g[k])return memo_g[k];
		if(k==0)return memo_g[k]=big_1;
		if(k==1)return memo_g[k]=new BigNumber(a(1));
		return memo_g[k]=g(k-1).times(a(k)).plus(g(k-2));
	}
	var memo_a={};
	var a=function(k)
	{			
		return memo_a[k]||(memo_a[k]=r(k).floor());
	}
	var memo_b={};
	var b=function(k)
	{
		if(k==0)return memo_b[k]=new BigNumber(0);
		return memo_b[k]||(memo_b[k]=d(k-1).times(a(k-1)).minus(b(k-1)));
	}
	var memo_d={};
	var d=function(k)
	{	
		if(k==0)return memo_d[k]=new BigNumber(1);
		return memo_d[k]||(memo_d[k]=(D_big.minus(b(k).pow(2))).div(d(k-1)));
	}
	var memo_r={};	
	var r=function(k)
	{	
		return memo_r[k]||(memo_r[k]=(D_sqrt.plus(b(k))).div(d(k)));
	}	
	var j=0;
	while(!((h(j).toPower(2).minus(D_big.times(g(j).toPower(2)))).equals(big_1)))
	{
		j++;
	}
	return h(j).toNumber();	
}

function minimal_solution2(N)
{
	if(N==0)return 0;
	var N_sqrt=Math.sqrt(N);
	var D_sqrt=Math.sqrt(N);
	var is_square=Number.isInteger(N_sqrt);
	if(is_square)return "None";
	var memo_h={};	
	var h=function(k)
	{
		if(memo_h[k])return memo_h[k];
		if(k==0)return memo_h[k]=a(0);
		if(k==1)return memo_h[k]=a(0)*a(1)+1;
		return memo_h[k]=h(k-1)*a(k)+h(k-2);
	}
	var memo_g={};
	var g=function(k)
	{
		if(memo_g[k])return memo_g[k];
		if(k==0)return memo_g[k]=1;
		if(k==1)return memo_g[k]=a(1);
		return memo_g[k]=g(k-1)*a(k)+g(k-2);
	}
	var memo_a={};
	var a=function(k)
	{			
		return memo_a[k]||(memo_a[k]=Math.floor(r(k)));
	}
	var memo_b={};
	var b=function(k)
	{
		if(k==0)return memo_b[k]=0;
		return memo_b[k]||(memo_b[k]=d(k-1)*a(k-1)-b(k-1));
	}
	var memo_d={};
	var d=function(k)
	{	
		if(k==0)return memo_d[k]=1;
		return memo_d[k]||(memo_d[k]=(N-Math.pow(b(k),2))/d(k-1));
	}
	var memo_r={};	
	var r=function(k)
	{	
		return memo_r[k]||(memo_r[k]=(N_sqrt+b(k))/d(k));
	}
	var j=0;
	while((Math.pow(h(j),2)-N*Math.pow(g(j),2))!=1)
	{		
		j++;
		if(j>10)break;
	}
	//console.log(N+","+j+","+h(j)+","+(Math.pow(h(j),2)-N*Math.pow(g(j),2)));
	return h(j);	
}
