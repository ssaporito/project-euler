function cyclical_figurate_sum(n)
{	
	n=parseInt(n);
	var min=1000;//Math.pow(10,n-1);
	var max=9999;//Math.pow(10,n)-1;
	var polygonal_list={};
	for(var k=3;k<=8;k++)
	{
		polygonal_list[k]={};
	}	
	var i=0;
	while(true)
	{
		var count_exceeded=0;
		for(var k=3;k<=8;k++)
		{
			var num=polygonal_number(i,k);			
			if((num>=min)&&(num<=max))
			{
				polygonal_list[k][num]=1;				
			}
			else{				
				if(num>max)
				{
					count_exceeded++;
				}
			}			
		}
		if(count_exceeded==6)
		{
			break;
		}
		i++;
	}

	var big_list={};
	for(var k=3;k<=8;k++)
	{
		for(var p in polygonal_list[k])
		{
			big_list[p]=(big_list[p])?(big_list[p].concat(k)):([k]);
		}
	}
	//console.log(find_adjacent_cycle(8128,3,big_list));
	//return 0;
	//console.log(big_list);
	var graph_after={};
	var graph_before={};
	var nodes=[];
	for(var num in big_list)
	{
		var indices=big_list[num];
		var indices_length=indices.length;
		for(var i=0;i<indices_length;i++)
		{
			var index=indices[i];
			var cycle_after=find_adjacent_cycle(num,index,big_list,true);
			var cycle_before=find_adjacent_cycle(num,index,big_list,false);
			if(cycle_after.length>0)
			{
				graph_after[[num,index]]=cycle_after;							
			}
			if(cycle_before.length>0)
			{
				graph_before[[num,index]]=cycle_before;
			}
		}
	}	

	var graphs=[graph_before,graph_after];
	for(var j in graphs)
	{		
		var graph=strongly_connected_graph(graphs[j],n);
		var wanted_cycle=find_wanted_cycle(graph,n);
		if(wanted_cycle)
		{
			return wanted_cycle.reduce(function(a,b){return a+parseInt(b.substr(0,b.length-2));},0);
		}
	}
}

function find_wanted_cycle(graph,n)
{
	var dfs_results={};
	for(var v in graph)
	{
		dfs_results[v]=dfs(graph,v);		
		var cycle_nodes=dfs_results[v]["cyclenodes"];		
		var cycle_found=cycle_nodes.length>0;
		if(cycle_found)
		{			
			var parents=dfs_results[v]["parents"];					
			for(var i in cycle_nodes)
			{			
				var cycle_node=cycle_nodes[i];
				var cycle=[];
				var curr_node=cycle_node;
				while((curr_node!=0))
				{
					cycle.push(curr_node);
					curr_node=parents[curr_node];
				}
				
				if(cycle.length==n)
				{
					var available_polys=[0,0,0,1,1,1,1,1,1];
					var wanted_cycle=true;
					for(var k in cycle)
					{
						var node=cycle[k];
						var node_poly=parseInt(node[node.length-1]);					
						if(available_polys[node_poly])
						{
							available_polys[node_poly]=0;
						}
						else
						{
							wanted_cycle=false;
							break;
						}
					}
					if(wanted_cycle)
					{	
						console.log("Wanted found!");
						console.log(cycle);
						return cycle;
					}
				}
			
			}
		}
	}
	return false;
}
function tarjan(adj_list,minimum_size)
{	
	var S=new Array();
	var indexes={};
	var lowlinks={};
	var onstack={};
	var components=[];
	for(var v in adj_list)
	{
		indexes[v]=null;
		lowlinks[v]=null;
		onstack[v]=false;
	}
	var index=0;
	for (var v in adj_list)
	{		
		if(indexes[v]==null)
		{
			strongconnect(v);
		}
	}
	function strongconnect(v)
	{
		indexes[v] = index;
		lowlinks[v] = index;
		index++;
		S.push(v);
		onstack[v] = true;
		
		var curr_neighbors=adj_list[v];
		for (var i in curr_neighbors)
		{
		    var w=curr_neighbors[i];
		    if (indexes[w]==null)
		    {
			    strongconnect(w);
			    lowlinks[v] = Math.min(lowlinks[v], lowlinks[w]);
		    }
		    else if (onstack[w])
		    {
		    	lowlinks[v] = Math.min(lowlinks[v], indexes[w])
		    }
		}

		if (lowlinks[v] == indexes[v])
		{
			var component={};
			while(w!=v)
			{
				w = S.pop();
				onstack[w]= false;
				var p=w.substr(0,4);
				var k=w.substr(w.length-1,1);
				component[p]=(component[p])?(component[p].concat(k)):([k]);
			}
			if(Object.keys(component).length>=minimum_size)
			{
				components.push(component);
			}
		}
	}
	return components;
}
function strongly_connected_graph(graph,minimum_component_size)
{
	var components=tarjan(graph,minimum_component_size);
	var new_graph={};
	for(var i in components)
	{
		var component=components[i];
		for(var p in component)
		{
			for(var p_i in component[p])
			{
				var k=component[p][p_i];
				var cycle=find_adjacent_cycle(p,k,component);
				if(cycle.length>0)
				{
					new_graph[[p,k]]=cycle;	
				}
			}
		}
	}
	return new_graph;
}	
function dfs(adj_list,node,end_node)
{
	var checked={};
	var parents={};
	var levels={};
	var explored={};
	var discovered=new Array();
	
	for(var v in adj_list)
	{
		checked[v]=false;
		parents[v]=-1;
		levels[v]=Number.POSITIVE_INFINITY;
		explored[v]=false;
	}
	parents[node]=0;
	levels[node]=0;
	explored[node]=true;
	discovered.push(node);
	var curr_level;
	var curr_node;
	var curr_neighbors;
	var end_found=false;
	var cycle_nodes=[];
	while ((discovered.length>0)&&(!end_found))
	{
		curr_node=discovered.pop();		
		curr_level=levels[curr_node]+1;
		if (!checked[curr_node])
		{
			checked[curr_node]=true;
			curr_neighbors=adj_list[curr_node];
			
			for (var i in curr_neighbors)
			{
				var w=curr_neighbors[i];			
				if (!explored[w])
				{
					parents[w]=curr_node;
					levels[w]=curr_level;
					explored[w]=true;									
				}
				if(w==node)
				{	
					cycle_nodes.push(curr_node);
				}	
				if (w==end_node)
				{								
					end_found=true;
					break;
				}
				discovered.push(w);
			}
		}
	}
	return {"checked":checked,"parents":parents,"levels":levels,"endfound":end_found,"cyclenodes":cycle_nodes};
}
function find_adjacent_cycle(num,poly,big_list,after_num)
{
	var common_two="";
	var max=Number.POSITIVE_INFINITY;
	if(after_num)
	{
		common_two=num.toString().substr(2,2);
		max=(parseInt(common_two)+1)*100;
	}
	else
	{
		common_two=num.toString().substr(0,2);
	}
	
	var list=[];
	for(var p in big_list)
	{
		if(p>max)
		{
			return list;
		}
		for(var i in big_list[p])
		{		
			var index=big_list[p][i];
			if(index!=poly)
			{
				if(p.toString().substr((after_num)?(0):(2),2)==common_two)
				{
					list.push([p,index].toString());
				}	
			}
		}	
	}
	return list;
}



