function sum_digit_factorials(n)
{
	n=parseInt(n);
	var sum=0;
	for(var k=10;k<n;k++)
	{
		var k_piece=k;
		var k_sum=0;
		while(k_piece>0)
		{
			var digit=k_piece%10;
			k_piece-=digit;
			k_piece/=10;
			k_sum+=factorial(digit);
		}
		if(k_sum===k)
		{
			sum+=k_sum;
		}		
	}
	return sum;
}