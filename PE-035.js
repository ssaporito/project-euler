function count_circular_primes(n)
{
	n=parseInt(n);
	return circular_primes_list(n).length;
}

function circular_primes_list(n)
{
	var max_value=n;
	var sieve={};
	var sqrt_max_value=Math.sqrt(max_value);
	
	for (var i = 2; i <= max_value; i++) 
	{
		sieve[i] = 1;
	}
	for(var i=2;i<=max_value;i++)
	{	
		if(sieve[i]==1)
		{			
			if (i<sqrt_max_value)
			{
				for(var j=Math.pow(i,2);j<=max_value;j+=i)
				{	
					sieve[j]=0;			
				}
			}			
		}
	}
	
	for(var p=2;p<=max_value;p++)
	{	
		if(sieve[p]==1)
		{			
			var p_str=p.toString();
			var p_str_rot=left_rotate_string(p_str);
			var p_rot_list=[p_str];
			while(p_str_rot!=p_str)
			{
				p_rot_list.push(p_str_rot);
				p_str_rot=left_rotate_string(p_str_rot);
			}
			var p_rot_list_length=p_rot_list.length;
			var circular=true;
			for(var k=0;k<p_rot_list_length;k++)
			{
				if (sieve[parseInt(p_rot_list[k])]==0)
				{
					circular=false;
					break;
				}
			}
			if(!circular)
			{
				for(var k=0;k<p_rot_list_length;k++)
				{
					var removed_p=parseInt(p_rot_list[k]);
					if(removed_p>=p)
					{
						sieve[removed_p]=0;
					}
				}
			}
		}
	}
	var primes_list=[];
	for(var p=2;p<=max_value;p++)
	{
		if(sieve[p]==1)
		{
			primes_list.push(p);
		}
	}
	return primes_list;	
}