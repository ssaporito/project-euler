function sum_nonabundant(n) 
{	
	var upper_limit=n;//28123;
	var abundant_list=[];
	for(var k=12;k<upper_limit;k++)
	{
		if(sum_proper_divisors(k)>k)
		{
			abundant_list.push(k);			
		}
	}	
	var abundant_summable={};
	var abundant_list_length=abundant_list.length;
	for (var ab1=0;ab1<abundant_list_length;ab1++)
	{
		for (var ab2=ab1;ab2<abundant_list_length;ab2++)
		{			
			var sum=abundant_list[ab1]+abundant_list[ab2];			
			abundant_summable[sum]=1;
		}
	}
	var result=0;
	for(var k=1;k<upper_limit;k++)
	{
		if(!abundant_summable[k])
		{
			result+=k;
		}
	}
	return result;
}