function sum_diagonals_spiral(n)
{
	n=parseInt(n);
	return sum_diagonals_spiral_v2(n);
}

function sum_diagonals_spiral_v1(n)
{
	if((n<=0)||(n%2==0))return -1;
	var sum=1;
	if(n==1) return sum;
	var limit=n;
	for(var m=3;m<=limit;m+=2)
	{
		sum+=4*Math.pow(m,2)-6*(m-1);
	}
	return sum;
}

function sum_diagonals_spiral_v2(n)
{
	if(n%2==0)return -1;
	var m=(n+1)/2;
	var sum=(16*(m)*(m+1)*(2*m+1)/6)-(28*(m)*(m+1)/2)+(16*(m))-3;
	if(sum>Math.pow(2, 53)-1)
	{	
		console.log("Warning: sum over max safe integer ("+Number.MAX_SAFE_INTEGER+")");
	}
	return sum;		
}