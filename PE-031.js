function count_coin_sums(value)
{
	value=parseInt(value);
	var memo={};
	var count_possible_sums=function(value,sum,coins,coins_index) // memoized function
	{
		if((memo[sum])&&(memo[sum][coins_index]))
		{
				return memo[sum][coins_index];
		}
		else
		{
			memo[sum]={};
			if(value===sum)return memo[sum][coins_index]=1;
			if(value<sum)return memo[sum][coins_index]=0;
			var coins_length=coins.length;
			
			var count=0;
			for(var k=coins_index;k<coins_length;k++)
			{
				count+=count_possible_sums(value,sum+coins[k],coins,k);
			}
			return memo[sum][coins_index]=count;
		}
	}
	var coins=[200,100,50,20,10,5,2,1];
	var result=count_possible_sums(value,0,coins,0);
	return result;
}

