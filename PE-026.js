function longest_recurring_cycle(n)
{
	var d=0;
	var sieve=order_sieve(n);
	//console.log(sieve);
	return sieve["d"];
}

function order_sieve(n)
{
	var limit=n;
	var primes=primes_list(n);
	var primes_length=primes.length;
	var order_sieve={};
	var max_k_element=-1;
	//console.log(primes);
	for(var i=0;i<primes_length;i++)
	{
		var p=primes[i];
		if((p==2)||(p==5))
		{
			order_sieve[p]=0;
		}
		else if(p==3)
		{			
			order_sieve[3]=1;
			var k=2;
			var max_power=Math.floor(base_log(3,n));
			while(k<=max_power)
			{
				var power=Math.pow(3,k);
				order_sieve[power]=Math.pow(3,k-2);
				if(order_sieve[power]>max_k_element)
				{
					max_k_element=power;
				}
				k++;
			}
		}
		else
		{
			var k=1;
			order_sieve[p]=discrete_log_of_prime(10,p);
			var max_power=Math.floor(base_log(p,n));
			while(k<=max_power)
			{
				var power=Math.pow(p,k);
				order_sieve[power]=Math.pow(p,k-1)*order_sieve[p];
				if(order_sieve[power]>max_k_element)
				{
					max_k_element=power;
				}
				k++;
			}
		}
	}
	return {"sieve":order_sieve,"d":max_k_element};
}